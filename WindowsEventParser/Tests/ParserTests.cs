﻿using System;
using System.Collections.Generic;
using WindowsEventParser;
using NUnit.Framework;

namespace Tests
{

    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void NullStringTest()
        {
            List<string> nullTestString = null;
            EventParser parser = new EventParser();
            Assert.Throws<ArgumentException>(()=> parser.ParseEvents(nullTestString));
        }

        [Test]
        public void EmptyStringTest()
        {
            List<string> emptyTestString = new List<string>();
            emptyTestString.Add("");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(emptyTestString);
            Assert.IsEmpty(kasperskyObjectsList);
        }

        [Test]
        public void WrongFormatStringTest()
        {
            List<string> wrongFormatTestString = new List<string>();
            wrongFormatTestString.Add("these aren't the strings you're looking for");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(wrongFormatTestString);
            Assert.IsEmpty(kasperskyObjectsList);
        }

        [Test]
        public void SomeFieldsFilledTest()
        {
            List<string> testString = new List<string>();
            testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>44</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	Продукт запущен
Программа\Название:	Kaspersky Endpoint Security 10 для Windows
Программа\Путь:	
Пользователь:	WIN8-32\Admin (Активный пользователь)
Компонент:	Управление защитой
</Data></EventData></Event>");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(testString);
            Assert.AreEqual(kasperskyObjectsList[0].EventType, "Продукт запущен");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramName, "Kaspersky Endpoint Security 10 для Windows");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramPath, "");
            Assert.AreEqual(kasperskyObjectsList[0].User, @"WIN8-32\Admin (Активный пользователь)");
            Assert.AreEqual(kasperskyObjectsList[0].Component, "Управление защитой");
            Assert.AreEqual(kasperskyObjectsList[0].CheckParameters, "");
            Assert.AreEqual(kasperskyObjectsList[0].ResultDescription, "");
        }

        [Test]
        public void AllFieldsEmptyTest()
        {
            List<string> testString = new List<string>();
            testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>44</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	
Программа\Название:	
Программа\Путь:	
Пользователь:	
Компонент:	
Результат\Описание:  
Параметры проверки:  
</Data></EventData></Event>");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(testString);
            Assert.AreEqual(kasperskyObjectsList[0].EventType, "");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramName, "");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramPath, "");
            Assert.AreEqual(kasperskyObjectsList[0].User, "");
            Assert.AreEqual(kasperskyObjectsList[0].Component, "");
            Assert.AreEqual(kasperskyObjectsList[0].CheckParameters, "");
            Assert.AreEqual(kasperskyObjectsList[0].ResultDescription, "");
        }


        [Test]
        public void AllFieldsFilledTest()
        {
            List<string> testString = new List<string>();
            testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>44</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	Продукт запущен
Программа\Название:	Kaspersky Endpoint Security 10 для Windows
Программа\Путь:	c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\
Пользователь:	WIN8-32\Admin (Активный пользователь)
Компонент:	Управление защитой
Результат\Описание:	Программа не активирована
Параметры проверки:	Уровень безопасности: Рекомендуемый, Эвристический анализ: Средний, Действие при обнаружении угрозы: Выбирать действие автоматически
</Data></EventData></Event>");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(testString);
            Assert.AreEqual(kasperskyObjectsList[0].EventType, "Продукт запущен");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramName, "Kaspersky Endpoint Security 10 для Windows");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramPath, @"c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\");
            Assert.AreEqual(kasperskyObjectsList[0].User, @"WIN8-32\Admin (Активный пользователь)");
            Assert.AreEqual(kasperskyObjectsList[0].Component, "Управление защитой");
            Assert.AreEqual(kasperskyObjectsList[0].CheckParameters, "Уровень безопасности: Рекомендуемый, Эвристический анализ: Средний, Действие при обнаружении угрозы: Выбирать действие автоматически");
            Assert.AreEqual(kasperskyObjectsList[0].ResultDescription, "Программа не активирована");
        }

        [Test]
        public void AllFieldsMissingTest()
        {
            List<string> testString = new List<string>();
            testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>44</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>
</Data></EventData></Event>");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(testString);
            Assert.IsEmpty(kasperskyObjectsList);
        }

        [Test]
        public void MultipleEntriesTest()
        {
            List<string> testString = new List<string>();
            testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>44</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	Продукт запущен
Программа\Название:	Kaspersky Endpoint Security 10 для Windows
Программа\Путь:	c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\
Пользователь:	WIN8-32\Admin (Активный пользователь)
Компонент:	Управление защитой
</Data></EventData></Event>");
            testString.Add(
                @"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>2</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:37:18.000000000Z'/><EventRecordID>43</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	Нарушено лицензионное соглашение
Программа\Название:	Kaspersky Endpoint Security 10 для Windows
Программа\Путь:	c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\
Пользователь:	WIN8-32\Admin (Активный пользователь)
Компонент:	Управление защитой
Результат\Описание:	Программа не активирована
</Data></EventData></Event>");
           testString.Add(@"<Event xmlns='http://schemas.microsoft.com/win/2004/08/events/event'><System><Provider Name='avp'/><EventID Qualifiers='49154'>4660</EventID><Level>4</Level><Task>0</Task><Keywords>0x80000000000000</Keywords><TimeCreated SystemTime='2015-09-07T13:35:13.000000000Z'/><EventRecordID>42</EventRecordID><Channel>Kaspersky Event Log</Channel><Computer>WIN8-32.csdis.dit.nppgamma.ru</Computer><Security/></System><EventData><Data>Тип события:	Продукт остановлен
Программа\Название:	Kaspersky Endpoint Security 10 для Windows
Программа\Путь:	c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\
Пользователь:	WIN8-32\Admin (Активный пользователь)
Компонент:	Управление защитой
</Data></EventData></Event>");
            EventParser parser = new EventParser();
            var kasperskyObjectsList = parser.ParseEvents(testString);
            Assert.AreEqual(kasperskyObjectsList[0].EventType, "Продукт запущен");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramName, "Kaspersky Endpoint Security 10 для Windows");
            Assert.AreEqual(kasperskyObjectsList[0].ProgramPath, @"c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\");
            Assert.AreEqual(kasperskyObjectsList[0].User, @"WIN8-32\Admin (Активный пользователь)");
            Assert.AreEqual(kasperskyObjectsList[0].Component, "Управление защитой");
            Assert.AreEqual(kasperskyObjectsList[0].CheckParameters, "");
            Assert.AreEqual(kasperskyObjectsList[0].ResultDescription, "");

            Assert.AreEqual(kasperskyObjectsList[1].EventType, "Нарушено лицензионное соглашение");
            Assert.AreEqual(kasperskyObjectsList[1].ProgramName, "Kaspersky Endpoint Security 10 для Windows");
            Assert.AreEqual(kasperskyObjectsList[1].ProgramPath, @"c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\");
            Assert.AreEqual(kasperskyObjectsList[1].User, @"WIN8-32\Admin (Активный пользователь)");
            Assert.AreEqual(kasperskyObjectsList[1].Component, "Управление защитой");
            Assert.AreEqual(kasperskyObjectsList[1].CheckParameters, "");
            Assert.AreEqual(kasperskyObjectsList[1].ResultDescription, "Программа не активирована");

            Assert.AreEqual(kasperskyObjectsList[2].EventType, "Продукт остановлен");
            Assert.AreEqual(kasperskyObjectsList[2].ProgramName, "Kaspersky Endpoint Security 10 для Windows");
            Assert.AreEqual(kasperskyObjectsList[2].ProgramPath, @"c:\program files\kaspersky lab\kaspersky endpoint security 10 for windows sp1\");
            Assert.AreEqual(kasperskyObjectsList[2].User, @"WIN8-32\Admin (Активный пользователь)");
            Assert.AreEqual(kasperskyObjectsList[2].Component, "Управление защитой");
            Assert.AreEqual(kasperskyObjectsList[2].CheckParameters, "");
            Assert.AreEqual(kasperskyObjectsList[2].ResultDescription, "");
            
        }

    }
}
