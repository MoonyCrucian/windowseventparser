﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WindowsEventParser
{
    public class KasperskyEvent
    {
          /// <summary>
          /// Идентификатор. Генерируется базой данных
          /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

          /// <summary>
          /// Тип события
          /// </summary>
        public string EventType { get; set; }

          /// <summary>
          /// Программа\Название
          /// </summary>
        public string ProgramName { get; set; }

          /// <summary>
          ///  Программа\Путь
          /// </summary>
        public string ProgramPath { get; set; }

          /// <summary>
          /// Пользователь
          /// </summary>
        public string User { get; set; }

          /// <summary>
          /// Параметры проверки
          /// </summary>
        public string CheckParameters { get; set; }

          /// <summary>
          /// Компонент
          /// </summary>
        public string Component { get; set; }

          /// <summary>
          /// Результат\Описание
          /// </summary>
        public string ResultDescription { get; set; }
    }
}
