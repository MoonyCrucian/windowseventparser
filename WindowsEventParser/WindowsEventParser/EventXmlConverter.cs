﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;

namespace WindowsEventParser
{
    class EventXmlConverter
    {
        public List<string> GetXmlFromLogFile(string logPath)
        {
            List<string> xmlRecordsList = new List<string>();
            
            using (var reader = new EventLogReader(logPath, PathType.FilePath))
            {
                EventRecord record;
                while ((record = reader.ReadEvent()) != null)
                {
                    using (record)
                    {
                        xmlRecordsList.Add(record.ToXml());
                    }
                }
            }
            return xmlRecordsList;
        }
    }
}
