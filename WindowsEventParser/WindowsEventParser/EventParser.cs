﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace WindowsEventParser
{
    public class EventParser
    {

        private Dictionary<string, string> CreateFieldValueDicitionary()
        {
            var fieldValueDictionary = new Dictionary<string, string>()
            {
                {"Тип события:", ""},
                {@"Программа\Название:", ""},
                {@"Программа\Путь:", ""},
                {"Пользователь:", ""},
                {"Параметры проверки:", ""},
                {"Компонент:", ""},
                {@"Результат\Описание:", ""}
            };
            return fieldValueDictionary;
        }

        private string nodeName = "Data";

        private bool CheckRecordSuitability(string record)
        {
            return record.Contains("Kaspersky");
        }

        private string GetXmlNode(string xmlString)
        {
            try
            {
                XDocument dox = XDocument.Parse(xmlString);
                if (dox.Root != null)
                {
                    var node = dox.Root.Descendants().FirstOrDefault(n => n.Name.LocalName == nodeName);

                    if (node != null)
                        return node.Value;
                }
                
            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }

        private Dictionary<string, string> ParseNode(string nodeString)
        {
            var separateFields = nodeString.Split('\n');
            var fieldValueDictionary = CreateFieldValueDicitionary();
            foreach (var field in separateFields)
            {
                var nameValuePair = field.Split('\t');
                if (nameValuePair.Length > 1 && nameValuePair[0] != "")
                {
                    fieldValueDictionary[nameValuePair[0]] = nameValuePair[1];
                }
            }
            return fieldValueDictionary;
        }

        private KasperskyEvent CreateKasperskyEvent(Dictionary<string, string> fieldValueDictionary)
        {
            KasperskyEvent kasperskyEvent = new KasperskyEvent();
            kasperskyEvent.ProgramName = fieldValueDictionary[@"Программа\Название:"];
            kasperskyEvent.CheckParameters = fieldValueDictionary["Параметры проверки:"];
            kasperskyEvent.Component = fieldValueDictionary["Компонент:"];
            kasperskyEvent.EventType = fieldValueDictionary["Тип события:"];
            kasperskyEvent.ProgramPath = fieldValueDictionary[@"Программа\Путь:"];
            kasperskyEvent.ResultDescription = fieldValueDictionary[@"Результат\Описание:"];
            kasperskyEvent.User = fieldValueDictionary["Пользователь:"];
            return kasperskyEvent;
        }


        public List<KasperskyEvent> ParseEvents(List<string> xmlList)
        {
            if (xmlList == null)
                throw new ArgumentException();
            List<KasperskyEvent> resultList = new List<KasperskyEvent>();
            foreach (var xmlRecord in xmlList)
            {
                if (CheckRecordSuitability(xmlRecord))
                {
                    var xmlNodeString = GetXmlNode(xmlRecord);
                    if (xmlNodeString != "")
                    {
                        var parsedNodeDictionary = ParseNode(xmlNodeString);
                        var kasperskyEvent = CreateKasperskyEvent(parsedNodeDictionary);
                        if (kasperskyEvent != null)
                            resultList.Add(kasperskyEvent);
                    }
                }
            }
            return resultList;
        }
    }
}
