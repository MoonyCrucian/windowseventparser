﻿using System.Data.Entity;

namespace WindowsEventParser
{
    public class KasperskyContext : DbContext
    {
        public DbSet<KasperskyEvent> Events { get; set; }
    }
}
