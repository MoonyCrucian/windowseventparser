﻿using System;
using System.IO;

namespace WindowsEventParser
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                switch (args[0])
                {
                    case "write":
                        WriteEvents(args[1]);
                        Console.WriteLine("File successfully saved to database");
                        break;
                    case "read":
                        ReadEvents();
                        break;
                    default: Console.WriteLine("Invalid command line arguments (write %filepath% or read)");
                        break;
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Invalid command line arguments (write %filepath% or read)");
            }
            catch (IOException)
            {
                Console.WriteLine("File not found");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid file format");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadLine();
        }

        static void WriteEvents(string logFilePath)
        {
            EventXmlConverter provider = new EventXmlConverter();
            EventParser parser = new EventParser();
            var kasperskyEvents = parser.ParseEvents(provider.GetXmlFromLogFile(logFilePath));
            using (var dbContext = new KasperskyContext())
            {
                dbContext.Events.AddRange(kasperskyEvents);
                dbContext.SaveChanges();
            }
        }

        static void ReadEvents()
        {
            using (var dbContext = new KasperskyContext())
            {
                foreach (var kasperskyEvent in dbContext.Events)
                {
                    Console.WriteLine("Тип события: "+ kasperskyEvent.EventType);
                    Console.WriteLine(@"Программа\Название: "+ kasperskyEvent.ProgramName);
                    Console.WriteLine(@"Программа\Путь: "+ kasperskyEvent.ProgramPath);
                    Console.WriteLine("Пользователь: "+ kasperskyEvent.User);
                    Console.WriteLine("Параметры проверки: "+ kasperskyEvent.CheckParameters);
                    Console.WriteLine("Компонент: "+ kasperskyEvent.Component);
                    Console.WriteLine(@"Результат\Описание: " + kasperskyEvent.ResultDescription);
                    Console.WriteLine("----\n");

                }
            }
        }
    }
}
